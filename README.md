# Hostify

## Prerequisites

### Database

In the root of the Hostify project :

    mysql.server start
    mysql -u root -p < hostify_db.sql

## Installation

In the root of the Hostify project :

### Without composer.phar

#### Install composer with brew   
    brew install composer
    composer self-update

#### Install dependencies    
    cd hostify
    composer install

### With composer.phar

#### Download composer.phar

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

#### Install dependencies

    cd hostify
    php ../composer.phar install

## Launch the server

    bin/cake server -p 1337

## Check

    http://localhost:1337/pages/home

## Unit Test

### Data Base

    Create a new database 'hostify_db_test'

### App.config

    Config the test DataSources with the user, the password and 'hotify_db_test'

### Launch Unit Test

    vendor/bin/phpunit
