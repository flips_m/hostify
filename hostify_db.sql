-- MySQL Script generated by MySQL Workbench
-- Mon Jul  4 23:30:42 2016
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hostify_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hostify_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hostify_db` DEFAULT CHARACTER SET utf8 ;
USE `hostify_db` ;

-- -----------------------------------------------------
-- Table `hostify_db`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`users` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`files` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`files` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `type` VARCHAR(64) NULL,
  `size` BIGINT NULL,
  `is_folder` TINYINT(1) NOT NULL,
  `folder_id` INT NULL,
  `uploader_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_files_files1_idx` (`folder_id` ASC),
  INDEX `fk_files_users1_idx` (`uploader_id` ASC),
  CONSTRAINT `fk_files_files1`
    FOREIGN KEY (`folder_id`)
    REFERENCES `hostify_db`.`files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_users1`
    FOREIGN KEY (`uploader_id`)
    REFERENCES `hostify_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`chats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`chats` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`chats` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`messages` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`messages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(256) NOT NULL,
  `date` DATETIME NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_messages_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `hostify_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`chats_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`chats_users` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`chats_users` (
  `chat_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`chat_id`, `user_id`),
  INDEX `fk_chats_users_users1_idx` (`user_id` ASC),
  INDEX `fk_chats_users_chats1_idx` (`chat_id` ASC),
  CONSTRAINT `fk_chats_users_chats1`
    FOREIGN KEY (`chat_id`)
    REFERENCES `hostify_db`.`chats` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chats_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `hostify_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`chats_messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`chats_messages` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`chats_messages` (
  `chat_id` INT NOT NULL,
  `message_id` INT NOT NULL,
  PRIMARY KEY (`chat_id`, `message_id`),
  INDEX `fk_chats_messages_messages1_idx` (`message_id` ASC),
  INDEX `fk_chats_messages_chats1_idx` (`chat_id` ASC),
  CONSTRAINT `fk_chats_messages_chats1`
    FOREIGN KEY (`chat_id`)
    REFERENCES `hostify_db`.`chats` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chats_messages_messages1`
    FOREIGN KEY (`message_id`)
    REFERENCES `hostify_db`.`messages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hostify_db`.`files_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hostify_db`.`files_users` ;

CREATE TABLE IF NOT EXISTS `hostify_db`.`files_users` (
  `file_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`file_id`, `user_id`),
  INDEX `fk_files_users_users1_idx` (`user_id` ASC),
  INDEX `fk_files_users_files1_idx` (`file_id` ASC),
  CONSTRAINT `fk_files_users_files1`
    FOREIGN KEY (`file_id`)
    REFERENCES `hostify_db`.`files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `hostify_db`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
