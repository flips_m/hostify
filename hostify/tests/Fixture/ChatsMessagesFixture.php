<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ChatsMessagesFixture
 *
 */
class ChatsMessagesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'chat_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'message_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_chats_messages_messages1_idx' => ['type' => 'index', 'columns' => ['message_id'], 'length' => []],
            'fk_chats_messages_chats1_idx' => ['type' => 'index', 'columns' => ['chat_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['chat_id', 'message_id'], 'length' => []],
            'fk_chats_messages_chats1' => ['type' => 'foreign', 'columns' => ['chat_id'], 'references' => ['chats', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_chats_messages_messages1' => ['type' => 'foreign', 'columns' => ['message_id'], 'references' => ['messages', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'chat_id' => 1,
            'message_id' => 1
        ],
    ];
}
