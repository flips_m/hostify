<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilesUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilesUsersTable Test Case
 */
class FilesUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FilesUsersTable
     */
    public $FilesUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.files_users'
        /*'app.files',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats',
        'app.chats_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FilesUsers') ? [] : ['className' => 'App\Model\Table\FilesUsersTable'];
        $this->FilesUsers = TableRegistry::get('FilesUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FilesUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'file_id' => 1,
            'user_id' => 1
        ];

        $filesTable = TableRegistry::get('FilesUsers');
        $f = $filesTable->newEntity($data);
        $this->assertEmpty($f->errors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $data = [
            'file_id' => 1
        ];
        $filesTable = TableRegistry::get('FilesUsers');
        $f = $filesTable->newEntity($data);
        $this->assertNull($f->user_id);
    }
}
