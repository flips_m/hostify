<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChatsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChatsTable Test Case
 */
class ChatsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ChatsTable
     */
    public $Chats;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chats',
        /*'app.messages',
        'app.chats_messages',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats_users',
        'app.files',
        'app.files_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Chats') ? [] : ['className' => 'App\Model\Table\ChatsTable'];
        $this->Chats = TableRegistry::get('Chats', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Chats);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'name' => 'test name'
        ];

        $usersTable = TableRegistry::get('Chats');
        $u = $usersTable->newEntity($data);
        $this->assertNotNull($u);
        $this->assertNotEmpty($u->name);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $data = [
            'name' => 'default test name'
        ];

        $chatsTable = TableRegistry::get('Chats');
        $chats = $chatsTable->newEntity($data);
        $this->assertEmpty($chats->errors());

        $data = [
            'name' => ''
        ];
        // name empty
        $chats = $chatsTable->newEntity($data);
        $this->assertNotEmpty($chats->errors());
    }
}
