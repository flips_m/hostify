<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChatsMessagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChatsMessagesTable Test Case
 */
class ChatsMessagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ChatsMessagesTable
     */
    public $ChatsMessages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chats_messages',
        /*'app.chats',
        'app.messages',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats_users',
        'app.files',
        'app.files_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ChatsMessages') ? [] : ['className' => 'App\Model\Table\ChatsMessagesTable'];
        $this->ChatsMessages = TableRegistry::get('ChatsMessages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChatsMessages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'chat_id' => 1,
            'message_id' => 1
        ];

        $chatsMessagesTable = TableRegistry::get('ChatsMessages');
        $cm = $chatsMessagesTable->newEntity($data);
        $this->assertEmpty($cm->errors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $data = [
            'chat_id' => 1
        ];
        $chatsMessagesTable = TableRegistry::get('ChatsMessages');
        $cm = $chatsMessagesTable->newEntity($data);
        $this->assertNull($cm->message_id);
    }
}
