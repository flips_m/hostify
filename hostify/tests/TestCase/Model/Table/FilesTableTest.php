<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilesTable Test Case
 */
class FilesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FilesTable
     */
    public $Files;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.files'
        /*'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats',
        'app.chats_users',
        'app.files_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Files') ? [] : ['className' => 'App\Model\Table\FilesTable'];
        $this->Files = TableRegistry::get('Files', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Files);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'name' => 'default test name',
            'type' => '',
            'size' => 1000,
            'is_folder' => 0
        ];

        $filesTable = TableRegistry::get('Files');
        $f = $filesTable->newEntity($data);
        $this->assertNotNull($f);
        $this->assertNotEmpty($f->size);
        $this->assertEmpty($f->type);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $data = [
            'name' => 'default test name',
            'type' => 'zip',
            'size' => 1000,
            'is_folder' => 0
        ];

        $filesTable = TableRegistry::get('Files');
        $f = $filesTable->newEntity($data);
        $this->assertEmpty($f->errors());

        $data = [
            'name' => 'default test name',
            'type' => 'zip',
            'size' => 1000
        ];
        // folder test empty
        $f = $filesTable->newEntity($data);
        $this->assertNotEmpty($f->errors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $data = [
            'name' => 'default test name',
            'type' => 'zip',
            'size' => 1000,
            'is_folder' => 0
        ];
        $filesTable = TableRegistry::get('Files');
        $f = $filesTable->newEntity($data);
        $this->assertNull($f->folder_id);
    }
}
