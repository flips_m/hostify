<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        /*'app.chat_has_user',
        'app.user_has_file',
        'app.chats',
        'app.chats_users',
        'app.files',
        'app.files_users'*/
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'App\Model\Table\UsersTable'];
        $this->Users = TableRegistry::get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'username' => 'test name',
            'password' => 'test'
        ];

        $usersTable = TableRegistry::get('Users');
        $u = $usersTable->newEntity($data);
        $this->assertNotNull($u);
        $this->assertNotEmpty($u->username);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $data = [
            'username' => 'test name',
            'password' => 'test'
        ];

        $usersTable = TableRegistry::get('Users');
        $u = $usersTable->newEntity($data);
        $this->assertEmpty($u->errors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $data = [
            'username' => 'test name',
            'password' => ''
        ];
        // field empty
        $usersTable = TableRegistry::get('Users');
        $u = $usersTable->newEntity($data);
        $this->assertNotEmpty($u->errors());

        $data = [
            'username' => 'test name'
        ];
        // field missing
        $usersTable = TableRegistry::get('Users');
        $u = $usersTable->newEntity($data);
        $this->assertNotEmpty($u->errors());
    }
}
