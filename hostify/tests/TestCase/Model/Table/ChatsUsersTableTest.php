<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChatsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChatsUsersTable Test Case
 */
class ChatsUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ChatsUsersTable
     */
    public $ChatsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chats_users',
        /*'app.chats',
        'app.messages',
        'app.chats_messages',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.files',
        'app.files_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ChatsUsers') ? [] : ['className' => 'App\Model\Table\ChatsUsersTable'];
        $this->ChatsUsers = TableRegistry::get('ChatsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ChatsUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'chat_id' => 1,
            'user_id' => 1
        ];

        $chatsUsersTable = TableRegistry::get('ChatsUsers');
        $cu = $chatsUsersTable->newEntity($data);
        $this->assertEmpty($cu->errors());
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $data = [
            'chat_id' => 1
        ];
        $chatsUsersTable = TableRegistry::get('ChatsUsers');
        $cu = $chatsUsersTable->newEntity($data);
        $this->assertNull($cu->user_id);
    }
}
