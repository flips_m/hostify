<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MessagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MessagesTable Test Case
 */
class MessagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MessagesTable
     */
    public $Messages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.messages',
        /*'app.chats',
        'app.chats_messages',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats_users',
        'app.files',
        'app.files_users' */
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Messages') ? [] : ['className' => 'App\Model\Table\MessagesTable'];
        $this->Messages = TableRegistry::get('Messages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Messages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $data = [
            'text' => '',
            'date' => '21/12/2016'
        ];
        $messageTable = TableRegistry::get('Messages');
        $f = $messageTable->newEntity($data);
        $this->assertEmpty($f->text);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $data = [
            'text' => 'message text',
            'date' => '21/12/2016'
        ];
        $messageTable = TableRegistry::get('Messages');
        $f = $messageTable->newEntity($data);
        $this->assertNull($f->user_id);
    }
}
