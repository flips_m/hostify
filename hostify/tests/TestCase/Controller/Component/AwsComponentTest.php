<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AwsComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\AwsComponent Test Case
 */
class AwsComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\AwsComponent
     */
    public $Aws;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Aws = new AwsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Aws);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->assertNotNull($this->Aws);
    }
}
