<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ChatsMessagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ChatsMessagesController Test Case
 */
class ChatsMessagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chats_messages',
        /*'app.chats',
        'app.messages',
        'app.users',
        'app.chat_has_user',
        'app.user_has_file',
        'app.chats_users',
        'app.files',
        'app.files_users' */
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->assertTrue(true);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->assertTrue(true);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->assertTrue(true);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->assertTrue(true);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->assertTrue(true);
    }
}
