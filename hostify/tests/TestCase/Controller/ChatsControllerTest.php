<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ChatsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\ChatsController Test Case
 */
class ChatsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chats',
        'app.chats_users',
        'app.messages',
        'app.chats_messages',
        'app.users'
        /*
        'app.chat_has_user',
        'app.user_has_file',
        'app.files',
        'app.files_users' */
    ];

    private function Auth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'php',
                    'password' => 'php'
                ]
            ]
        ]);
    }

    public function setUp()
    {
        parent::setUp();
        $this->Auth();
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/chats');
        $this->assertResponseOk(); // check the server response on /chats url
        $this->assertTemplate('index'); // check the template renderer
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/chats/view/1'); // id of the current auth user
        $this->assertResponseOk();
        $this->assertTemplate('view');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $data = [
            'name' => 'test chat'
        ];

        $this->post('/chats/add', $data);
        $this->assertResponseSuccess();

        $chats = TableRegistry::get('Chats');
        $query = $chats->find();
        $this->assertEquals(2, $query->count());

        $chat = $chats->get(2);
        $this->assertEquals('test chat', $chat->name);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $data = [
            'name' => 'Group Hostify'
        ];

        $this->testAdd();

        $chats = TableRegistry::get('Chats');
        $old = $chats->get(2);
        $this->assertNotNull($old);

        $u = $chats->patchEntity($old, $data);
        $chats->save($u);

        $this->post('/chats/edit/2');
        $this->assertEquals($data['name'], $chats->get(2)->name);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->testAdd();

        $this->post('/chats/delete/2');
        $chats = TableRegistry::get('Chats');
        $query = $chats->find();
        $this->assertEquals(1, $query->count());
    }
}
