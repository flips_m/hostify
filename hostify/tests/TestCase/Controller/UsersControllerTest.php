<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        /*'app.chat_has_user',
        'app.user_has_file',
        'app.chats',
        'app.chats_users',
        'app.files',
        'app.files_users' */
    ];

    private function Auth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'php',
                    'password' => 'php'
                ]
            ]
        ]);
    }

    public function setUp()
    {
        parent::setUp();
        $this->Auth();
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/users');
        $this->assertResponseOk(); // check the server response on /users url
        $this->assertTemplate('index'); // check the template renderer
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/users/view/1'); // id of the current auth user
        $this->assertResponseOk();
        $this->assertTemplate('view');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $data = [
            'username' => 'test user',
            'password' => 'password'
        ];

        $this->post('/users/add', $data);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $query = $users->find();
        $this->assertEquals(2, $query->count());

        $u = $users->get(2);
        $this->assertEquals('test user', $u->username);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $data = [
            'username' => 'test new username',
            'password' => 'password'
        ];

        $this->testAdd();

        $users = TableRegistry::get('Users');
        $oldUser = $users->get(2);
        $this->assertNotNull($oldUser);

        $u = $users->patchEntity($oldUser, $data);
        $users->save($u);

        $this->post('/users/edit/2');
        $this->assertEquals($data['username'], $users->get(2)->username);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->testAdd();

        $this->post('/users/delete/2');
        $users = TableRegistry::get('Users');
        $query = $users->find();
        $this->assertEquals(1, $query->count());
    }
}
