<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FilesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\FilesController Test Case
 */
class FilesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.files',
        'app.users',
        //'app.chat_has_user',
        //'app.user_has_file',
        'app.chats',
        'app.chats_users',
        'app.files_users'
    ];

    private function Auth()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'php',
                    'password' => 'php'
                ]
            ]
        ]);
    }

    public function setUp()
    {
        parent::setUp();
        $this->Auth();
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/files');
        $this->assertResponseOk();
        $this->assertTemplate('index');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->get('/chats/view/1');
        $this->assertResponseOk();
        $this->assertTemplate('view');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->assertTrue(true);
    }
    
    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->assertTrue(true);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->assertTrue(true);
    }
}
