<?= $this->element('Navbar/files'); ?>
<div class="files index large-9 medium-8 columns content">
    <h3><?= __('Files') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('type') ?></th>
                <th><?= $this->Paginator->sort('size') ?></th>
                <th><?= $this->Paginator->sort('is_folder') ?></th>
                <th><?= $this->Paginator->sort('folder_id') ?></th>
                <th><?= $this->Paginator->sort('uploader_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($files as $file): ?>
            <tr>
                <td><?= $this->Number->format($file->id) ?></td>
                <td><?= h($file->name) ?></td>
                <td><?= h($file->type) ?></td>
                <td><?= $this->Number->format($file->size) ?></td>
                <td><?= h($file->is_folder) ?></td>
                <td><?= $file->folder_id ?></td>
                <td><?= $this->Number->format($file->uploader_id) ?></td>
                <td class="actions">
                    <?= $file->is_folder ? '' : $this->Html->link(__('Download'), ['action' => 'download', $file->id]) ?>
                    <?= $this->Html->link(__('Share'), ['controller' => 'FilesUsers', 'action' => 'share', $file->id]) ?>
                    <?= $this->Html->link(__('View'), ['action' => 'view', $file->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $file->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $file->id], ['confirm' => __('Are you sure you want to delete # {0}?', $file->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
