<?php
use Cake\Routing\Router;
?>

<?= $this->element("Navbar/files") ?>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    tempX = 0;
    tempY = 0;

    function getMouseXY(e) {
        tempX = e.pageX;
        tempY = e.pageY;
        // catch possible negative values in NS4
        if (tempX < 0) {
            tempX = 0;
        }
        if (tempY < 0) {
            tempY = 0;
        }
    }

    function contextMenu(id, folder) {
        var button = document.getElementById("button-download");
        button.set
        button.disabled = folder;
        button = document.getElementById("button-share");
        button.disabled = folder;
        button = document.getElementById("button-delete");
        button.disabled = folder;
        var elt = document.getElementById("context-menu");
        elt.setAttribute("value", id);
        getMouseXY(window.event);
        elt.style.top = tempY - 20 + "px";
        elt.style.left = tempX - 20 + "px";
    }

    function hide() {
        var elt = document.getElementById("context-menu");
        getMouseXY(window.event);
        elt.style.left = "-500px";
    }

    function download() {
        var id = document.getElementById("context-menu").getAttribute("value");
        window.location = "/files/download/" + id;
    }

    function deleteFile() {
        var id = document.getElementById("context-menu").getAttribute("value");
        $.ajax({
            type: "POST",
            url: '<?php
                echo Router::url(array('controller' => 'Files', 'action' => 'delete'));
                ?>',
            data: {
                id: id
            },
            complete: function () {
                window.location.reload();
            }
        });
    }

    function share () {
        var id = document.getElementById("context-menu").getAttribute("value");
        window.location = "/files-users/share/" + id;
    }

    function edit () {
        var id = document.getElementById("context-menu").getAttribute("value");
        window.location = "/files/edit/" + id;
    }

    $(function () {
        $(".draggable").draggable({
            handle: "img",
            containment: "parent",
            start: function () {
                document.getElementById(this.getAttribute("name")).style.zIndex = 9999; //bring to front, marche pas
            },
            drag: function () {
                getMouseXY(window.event);
                var div = document.getElementsByName('item');
                for (var i = 0; i < div.length; i++) {
                    if (div[i].getAttribute("is_folder") == "true" &&
                        div[i].id.startsWith("file-") &&
                        div[i].id != this.getAttribute('name')) {
                        var pos = div[i].getBoundingClientRect();
//                        document.getElementById('debug').innerText = "";
                        if (tempX > pos.left && tempX < pos.right && tempY > pos.top && tempY < pos.bottom) {
//                            document.getElementById('debug').innerText = div[i].id;
                            div[i].className = 'over_folder';
                            break;
                        }
                    }
                }
//                document.getElementById('mouse').innerText = tempX + ", " + tempY;
            },
            stop: function () {
                getMouseXY(window.event);
                var dst = null;
                var div = document.getElementsByName('item');
                document.getElementById(this.getAttribute("name")).style.zIndex = 0;
                for (var i = 0; i < div.length; i++) {
                    if (div[i].id.startsWith("file-") && div[i].id != this.getAttribute('name')) {
                        var pos = div[i].getBoundingClientRect();
                        if (tempX > pos.left && tempX < pos.right && tempY > pos.top && tempY < pos.bottom) {
                            if (div[i].getAttribute("is_folder") == "true") {
                                dst = div[i].id;
                            }
                            break;
                        }
                    }
                }
                if (dst != null) {
                    var src = this.getAttribute("name");
                    $.ajax({
                        type: "POST",
                        url: '<?php
                            echo Router::url(array('controller' => 'Files', 'action' => 'moveFile'));
                            ?>',
                        data: {
                            src: src.substr(5),
                            dst: dst.substr(5)
                        },
                        complete: function () {
                            location.reload(true);
                        }
                    });
                }
            }
        });
    });
    </script>

    <div class = "files view large-9 medium-8 columns content" >
        <h2><?= $folder_name ?></h2>
        <div id='files'>
        <?php
        if ($id != 0) {
            echo $this->Html->image("http://image.flaticon.com/icons/svg/137/137531.svg",
                [
                    "is_folder" => "true",
                    "name" => "item",
                    "id" => "file-$parent_id",
                    "width" => 70,
                    "height" => 70,
                    "alt" => "",
                    "url" => ['action' => 'browse', $parent_id]
                ]);
            echo "<br/>";
        }
        foreach ($files as $key => $value) {
            echo "<div name='file-$key' class='draggable ui-state-default'>";
            echo $this->Html->image($value[2],
                [
                    "oncontextmenu" => "contextMenu($key, " . ($value[0] ? 1 : 0) . ");return false;",
                    "title" => $value[1],
                    "is_folder" => $value[0] ? "true" : "false",
                    "name" => "item",
                    "id" => "file-$key",
                    "width" => 100,
                    "height" => 100,
                    "alt" => "",
                    "url" => $value[0] ? ['action' => 'browse', $key] : null
                ]);
            $name = $value[1];
//    if (strlen($value[1]) > 10) {
//        $name = substr($name, 0, 10) . "...";
//    }
            echo "<br/><figcaption title='$value[1]'>$name</figcaption>";
            echo "</div>";
        }
        ?>
        </div>
        </div>
        <div
    id="context-menu"
    style="position: absolute;
    left: -500px;"
    align="center"
    value="0"
    onmouseleave="hide()">
        <button
    id="button-download"
    onclick="download();">
        Télécharger
        </button>
        <br/>
        <button
    id="button-rename"
    onclick="edit()">
        Éditer
        </button>
        <br/>
        <button
    id="button-share"
    onclick="share()">
        Partager
        </button>
        <br/>
        <button
    id="button-delete"
    onclick="deleteFile()">
        Supprimer
        </button>
        </div>