<!-- src/Template/Users/login.ctp -->

<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __("Please enter your username and password") ?></legend>
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
    </fieldset>
<?= $this->Form->button(__('Log in')); ?>
<?= $this->Form->end() ?>
</div>
<div>
<?= $this->Html->link(__('Sign up'), ['action' => 'add']); ?>
</div>