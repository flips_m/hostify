<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Upload'), ['controller' => 'Files', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Add Folder'), ['controller' => 'Files', 'action' => 'addFolder']) ?></li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
        <li class="heading"><?= __('Navigation') ?></li>
        <li><?= $this->Html->link(__('Browse'), ['controller' => 'Files', 'action' => 'browse', 0]) ?></li>
        <li><?= $this->Html->link(__('Shared with me'), ['controller' => 'FilesUsers' ,'action' => 'index']) ?></li>
    </ul>
</nav>