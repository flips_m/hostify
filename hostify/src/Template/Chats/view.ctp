<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chat'), ['action' => 'edit', $chat->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chat'), ['action' => 'delete', $chat->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chat->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chats'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chat'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?></li>
    </ul>
</nav>
<div class="chats view large-9 medium-8 columns content">
    <h3><?= h($chat->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($chat->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($chat->id) ?></td>
        </tr>
    </table>

    <div class="related">
        <h4><?= __('Chat members') ?></h4>
        <?php if (!empty($chat->users)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('Username') ?></th>
                </tr>
                <?php foreach ($chat->users as $users): ?>
                    <tr>
                        <td><?= h($users->id) ?></td>
                        <td><?= h($users->username) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Chat') ?></h4>
        <?php if (!empty($chat->messages)): ?>
        <table cellpadding="0" cellspacing="0" id="msgTable">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Message') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Author') ?></th>
            </tr>

            <?php foreach ($chat->messages as $messages): ?>
            <tr>
                <td><?= h($messages->id) ?></td>
                <td><?= h($messages->text) ?></td>
                <td><?= h($messages->date) ?></td>
                <td>
                    <?php
                    $found = false;
                    foreach ($chat->users as $value) {
                        if ($value->id == $messages->user_id) {
                            echo $value->username;
                            $found = true;
                            break;
                        }
                    }
                    if (!$found) {
                        echo h($messages->user_id);
                    }
                    ?>
                </td>
            </tr>

            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <?= $this->Form->create('Messages') ?>
        <fieldset>
            <legend><?= __('Send Messsage') ?></legend>
            <?php
            echo $this->Form->input('text');
            ?>
        </fieldset>
        <?= $this->Form->button(__('Send')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      refreshTable();
    });

    function refreshTable(){
        $.ajax({
            type: 'POST',
            cache: false,
            url: '<?php use Cake\Routing\Router;
                echo Router::url(array('controller' => 'Chats', 'action' => 'table'));
                ?>/<?php echo $chat->id; ?>',
            success: function(chat){
                var o = JSON.parse(chat);

                var tbl_body = "<tr><th>Id</th><th>Message</th><th>Date</th><th>Author</th></tr>";
                $.each(o.messages, function() {
                    var tbl_row = "";
                    $.each(this, function(k , v) {
                        if (k == "id" || k == "text" || k == "date") {
                            tbl_row += "<td>" + v + "</td>";
                        } else if (k == "user_id") {
                            var found = false;
                            $.each(o.users, function(k2, user) {
                                if (v == user.id) {
                                    tbl_row += "<td>" + user.username + "</td>";
                                    found = true;
                                }
                            })
                            if (!found) {
                                tbl_row += "<td>" + v + "</td>";
                            }
                        }
                    })
                    tbl_body += "<tr>"+tbl_row+"</tr>";
                })

                $("#msgTable").html(tbl_body);
                setTimeout(refreshTable, 1000);
            },
            error: function() {
                //alert('KO');
            }
        })
    }
</script>


