<?= $this->element('Navbar/files'); ?>
<div class="filesUsers index large-9 medium-8 columns content">
    <h3><?= __('Shared with me') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('file_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($filesUsers as $filesUser): ?>
            <tr>
                <td><?= $filesUser->has('file') ? $this->Html->link($filesUser->file->name, ['controller' => 'Files', 'action' => 'view', $filesUser->file->id]) : '' ?></td>
                <td><?= $filesUser->has('user') ? $this->Html->link($filesUser->user->username, ['controller' => 'Users', 'action' => 'view', $filesUser->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Download'), ['controller' => 'Files', 'action' => 'download', $filesUser->file_id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
