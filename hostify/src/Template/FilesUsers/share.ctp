<?= $this->element('Navbar/files'); ?>
<div class="filesUsers index large-9 medium-8 columns content">
    <h3><?= __('Shared to') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('user_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($filesUsers as $filesUser): ?>
            <tr>
                <td><?= $filesUser->has('user') ? $this->Html->link($filesUser->user->username, ['controller' => 'Users', 'action' => 'view', $filesUser->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $filesUser->file_id, $filesUser->user_id ], ['confirm' => __('Are you sure you want to delete access ?')]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
<div class="filesUsers form large-9 medium-8 columns content">
    <?= $this->Form->create('Share') ?>
    <fieldset>
        <legend><?= __('Add user') ?></legend>
        <?php
        echo $this->Form->input('username', ['type' => 'text']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
