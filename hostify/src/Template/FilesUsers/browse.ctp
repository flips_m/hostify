<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        
    </ul>
</nav>
<div class="files view large-9 medium-8 columns content">
    <h2><?= $folder_name ?></h2>
    <?php
    if ($id != 0) {
        echo $this->Html->link(__("←"), ['action' => 'browse', $parent_id]);
        echo "<br/>";
    }
    echo "<div style='float: left'>";
    foreach ($files as $key => $value) {
        echo "<div>";
            echo $this->Html->image($value[2],
                [
                    "width" => 100,
                    "height" => 100,
                    "url" => ['action' => 'browse', $key]
                ]);
        echo "<br/>" . $value[1];
        echo "</div>";
    }
    ?>
</div>