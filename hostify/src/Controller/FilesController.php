<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Event\Event;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 * @property \App\Controller\Component\AwsComponent $Aws
 */
class FilesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Aws');
        $this->loadComponent('Security');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $files = $this->paginate($this->Files);

        $this->set(compact('files'));
        $this->set('_serialize', ['files']);
    }

    /**
     * View method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $file = $this->Files->get($id);

        $this->set('file', $file);
        $this->set('_serialize', ['file']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

    }
    
    public function drop() {
        $file = $this->Files->newEntity();
            $metadata = $this->request->data['file'];
            $file = $this->Files->patchEntity($file, $metadata);
            $file->is_folder = false;
            $file->uploader_id = $this->Auth->user('id');
            $file = $this->Files->save($file);
            if ($file) {
                $result = $this->Aws->putObject($file->id, $file->name, $metadata['tmp_name']);
                if ($result) {
                    return $this->redirect(['controller' => 'FilesUsers', 'action' => 'add', $file->id, $file->uploader_id, $file->uploader_id]);
                } else {
                    $this->Files->delete($file);
                }
            }
        $this->set(compact('file'));
        $this->set('_serialize', ['file']);
    }

    /**
     * Add Folder method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addFolder()
    {
        $file = $this->Files->newEntity();
        if ($this->request->is('post')) {
            $file = $this->Files->patchEntity($file, $this->request->data);
            $file->is_folder = true;
            $file->uploader_id = $this->Auth->user('id');
            if ($this->Files->save($file)) {
                    return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The file could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('file'));
        $this->set('_serialize', ['file']);
    }

    /**
     * Edit method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $file = $this->Files->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $file = $this->Files->patchEntity($file, $this->request->data);
            if ($this->Files->save($file)) {

                return $this->redirect(['action' => 'browse', $file->folder_id == null ? 0 : $file->folder_id]);
            } else {
                $this->Flash->error(__('The file could not be saved. Please, try again.'));
            }
        }
        $files = $this->Files->Files->find('list', ['limit' => 200])->where('is_folder = 1');
        $users = $this->Files->Users->find('list', ['limit' => 200]);
        $this->set(compact('file', 'files', 'users'));
        $this->set('_serialize', ['file']);
    }

    public function browse($id = null) {
        $parent_id = null;
        $folder_name = "Racine";
        //<editor-fold desc="ID parent/Nom dossier courant">
        if ($id != 0) {
            $sql_parent = $this->Files->Files
                ->find()
                ->select(['name', 'folder_id'])
                ->where("id = $id");
            foreach ($sql_parent as $item) {
                $parent_id = $item->folder_id;
                $folder_name = $item->name;
                break;
            }
        }
        if ($id != 0 && $parent_id == null)
            $parent_id = 0;
        //</editor-fold>
        //<editor-fold desc="Liste dossiers/fichiers">
        $condition = $id == 0 ?
            "isnull(folder_id)" :
            "folder_id = $id";
        $condition .= " and uploader_id = " . $this->Auth->user('id');
        $sql_files = $this->Files->Files
            ->find()
            ->where($condition);
        $files = array();
        foreach ($sql_files as $sql_file) {
            $files[$sql_file->id] = array();
            if ($sql_file->is_folder) {
                $sub_files = $this->Files->Files
                    ->find()
                    ->where("folder_id = $sql_file->id and uploader_id = " . $this->Auth->user('id'))
                ->count();
            }
            $files[$sql_file->id] = [
                $sql_file->is_folder,
                $sql_file->name,
                $sql_file->is_folder ?
                    ($sub_files == 0 ?
                        "http://image.flaticon.com/icons/svg/148/148953.svg" :
                        "http://image.flaticon.com/icons/svg/148/148955.svg") :
                    "http://image.flaticon.com/icons/svg/148/148964.svg"
            ];
        }
        //</editor-fold>
        $this->set(compact('id', 'files', 'parent_id', 'folder_name'));
        $this->set('_serialize', ['id', 'files', 'parent_id', 'folder_name']);
    }

    /**
     * Delete method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        if ($id == null)
            $id = $this->request->data("id");
        $this->request->allowMethod(['post', 'delete']);
        $file = $this->Files->get($id);
        if ($file->uploader_id == $this->Auth->user('id')) {
            if ($this->Files->delete($file)) {
                $this->Aws->deleteObject($file->id);
                $this->Flash->success(__('The file has been deleted.'));
            } else {
                $this->Flash->error(__('The file could not be deleted. Please, try again.'));
            }
        }
        return $this->redirect(['action' => 'index']);

    }

    public function download($id = null)
    {
        $this->loadModel('FilesUsers');

        $user_id = $this->Auth->user('id');

        $checkUser = $this->FilesUsers->find('all')
            ->where(([ 'file_id' => $id, 'user_id' => $user_id]));

        if (!empty($checkUser->toArray())) {
            $callFromAjax = $id == null;
            if ($id == null)
                $id = $this->request->data("id");
            $metadata = $this->Files->get($id);
            $result = $this->Aws->getObject($metadata->id, $metadata->name);
            if ($callFromAjax)
                echo $result;
            return $this->redirect($result);
        } else
            $this->redirect($this->referer());
    }

    public function moveFile() {
        $src = $this->request->data("src");
        $dst = $this->request->data("dst");
        if ($dst == 0)
            $dst = null;
        $file = $this->Files
            ->get($src);
        $file->folder_id = $dst;
        if ($this->Files->save($file)) {
            $this->Flash->success(__('The file has been moved.'));
        } else {
            $this->Flash->error(__('The file could not be moved. Please, try again.'));
        }
        return true;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->config('unlockedActions',
            [
                'drop',
                'moveFile',
                'download',
                'delete'
            ]);
    }


}
