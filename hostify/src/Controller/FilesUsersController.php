<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

/**
 * FilesUsers Controller
 *
 * @property \App\Model\Table\FilesUsersTable $FilesUsers
 */
class FilesUsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $id = $this->Auth->user('id');
        $query = $this->FilesUsers->find('all')->contain([
            'Files' => function ($q) {
                $id = $this->Auth->user('id');
                return $q->find('all')->where(['Files.uploader_id !=' => $id]);},
            'Users'
        ])->where(['user_id =' => $id, ]);

        $filesUsers = $this->paginate($query);

        $this->set(compact('filesUsers'));
        $this->set('_serialize', ['filesUsers']);
    }

    /**
     * Add method
     *
     * @param string $file_id File id.
     * @param string $uploader_id Uploader id.
     * @param string $user_id User id.
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($file_id, $uploader_id, $user_id)
    {
        $filesUser = $this->FilesUsers->newEntity();
        $checkUser = $this->Auth->user('id') == $uploader_id;
        if ($checkUser) {
            $filesUser->file_id = $file_id;
            $filesUser->user_id = $user_id;
            if ($this->FilesUsers->save($filesUser)) {
                if ($user_id == $uploader_id)
                    return $this->redirect(['controller' => 'Files', 'action' => 'index', $file_id]);
                $this->Flash->success(__('The file has been share.'));
                return $this->redirect(['action' => 'share', $file_id]);
            } else {
                if ($user_id == $uploader_id)
                    return $this->redirect(['controller' => 'Files', 'action' => 'delete', $file_id]);
                return $this->redirect(['action' => 'share', $file_id]);
            }
        } else {
            $this->Flash->error(__('You\'re not the uploader. The file could not be share.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $file_id File id.
     * @param string|null $user_id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($file_id = null, $user_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $filesUser = $this->FilesUsers->get([$file_id, $user_id], [
                'contain' => ['Files']
        ]);
        if ($filesUser->file->uploader_id != $filesUser->user_id) {
            if ($this->FilesUsers->delete($filesUser)) {
                $this->Flash->success(__('The permission has been deleted.'));
            } else {
                $this->Flash->error(__('The permission could not be deleted. Please, try again.'));
            }
        } else
            $this->Flash->error(__('You\'re the uploader of this file. The permission could not be deleted.'));
        return $this->redirect(['action' => 'index']);
    }

    public function browse($id = null)
    {
        $parent_id = null;
        $folder_name = "Racine";
        //<editor-fold desc="ID parent/Nom dossier courant">
        if ($id != 0) {
            $sql_parent = $this->FilesUsers->Files
                ->find()
                ->select(['name', 'folder_id'])
                ->where("id = $id");
            foreach ($sql_parent as $item) {
                $parent_id = $item->folder_id;
                $folder_name = $item->name;
                break;
            }
        }
        if ($id != 0 && $parent_id == null)
            $parent_id = 0;
        //</editor-fold>
        //<editor-fold desc="Liste dossiers/fichiers">
        $condition = $id == 0 ?
            "isnull(folder_id)" :
            "folder_id = $id";
        $sql_files = $this->FilesUsers->Files
            ->find()
            ->where($condition);
        $files = array();
        foreach ($sql_files as $sql_file) {
            $files[$sql_file->id] = array();
            if ($sql_file->is_folder) {
                $sub_files = $this->FilesUsers->Files
                    ->find()
                    ->where("folder_id = $sql_file->id")
                    ->count();
            }
            $files[$sql_file->id] = [
                $sql_file->is_folder,
                $sql_file->name,
                $sql_file->is_folder ?
                    ($sub_files == 0 ?
                        "http://image.flaticon.com/icons/svg/148/148953.svg" :
                        "http://image.flaticon.com/icons/svg/148/148955.svg") :
                    "http://image.flaticon.com/icons/svg/148/148964.svg"
            ];
        }
        //</editor-fold>
        $this->set(compact('id', 'files', 'parent_id', 'folder_name'));
    }

    public function share($id) {

        $file = $this->FilesUsers->Files->get($id);
        $uploader_id = $this->Auth->user('id');

        if ($file->uploader_id == $uploader_id) {
            $query = $this->FilesUsers->find('all')->contain([
                'Users'
            ])->where(['file_id =' => $id, 'user_id !=' => $uploader_id]);

            $filesUsers = $this->paginate($query);


            if ($this->request->is('post')) {
                $username = $this->request->data['username'];
                $user = $this->FilesUsers->Users->find('all')->where(['Users.username' => $username])->first();
                if ($user) {
                    return $this->redirect(['action' => 'add', $id, $uploader_id, $user->id]);
                } else {
                    $this->Flash->error(__('User not found.'));
                }
            }

            $this->set(compact('filesUsers'));
            $this->set('_serialize', ['filesUsers']);
        } else
            return $this->redirect($this->referer());
    }
}
