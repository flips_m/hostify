<?php
namespace App\Controller\Component;

use Aws\Sts\StsClient;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Aws\S3\S3Client;

/**
 * Aws component
 * @property \Aws\S3\S3Client $_s3
 */
class AwsComponent extends Component
{
    const BUCKET_NAME = "hostify-venner";

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->_s3 = new S3Client([
            'version' => '2006-03-01',
            'region' => 'eu-west-1'
        ]);
    }

    public function putObject($key, $filename, $pathToFile) {
        $result = $this->_s3->putObject([
                'Bucket'     => self::BUCKET_NAME,
                'Key'        => $key,
                'SourceFile' => $pathToFile,
                'ContentDisposition' => "attachment;filename={$filename}"
            ]
        );
        return $result;
    }

    public function deleteObject($key) {
        $result = $this->_s3->deleteObject([
                'Bucket'     => self::BUCKET_NAME,
                'Key'        => $key,
            ]
        );
        return $result;
    }
    
    public function getObject($key, $filename) {
        $cmd = $this->_s3->getCommand('GetObject', [
            'Bucket' => self::BUCKET_NAME,
            'Key'    => $key
        ]);

        $request = $this->_s3->createPresignedRequest($cmd, '+1 minutes');

        $presignedUrl = (string) $request->getUri();
        return $presignedUrl;
    }
}
