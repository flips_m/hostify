<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Chats Controller
 *
 * @property \App\Model\Table\ChatsTable $Chats
 */
class ChatsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('ChatsUsers');

        $chatsUsers = $this->ChatsUsers->find()
            ->select(['chat_id'])
            ->where(['user_id' => $this->Auth->user('id')])
            ->toArray()
        ;
        $chatsId = array_map(create_function('$o', 'return (int)$o->chat_id;'), $chatsUsers);

        if (empty($chatsId)) {
            $chats = $this->Chats->find()
                ->where(['id' => -1]);
        } else {
            $chats = $this->Chats->find()
                ->where(['Chats.id IN' => $chatsId]);
        }

        $chats = $this->paginate($chats);

        $this->set(compact('chats'));
        $this->set('_serialize', ['chats']);
    }

    /**
     * View method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->isChatPrivate($id)) {
            $this->Flash->error(__('You don\'t belong to this chat. You can\'t access to the chat.'));
            return $this->redirect(['action' => 'index']);
        }

        $chat = $this->Chats->get($id, [
            'contain' => ['Messages', 'Users']
        ]);

        $this->set('chat', $chat);
        $this->set('_serialize', ['chat']);

        // post message
        if ($this->request->is('post')) {
            $this->loadModel('Messages');
            $message = $this->Messages->newEntity();
            $message = $this->Messages->patchEntity($message, $this->request->data);
            $this->loadModel('Chats');
            $chat = $this->Chats->get($id, [
                'contain' => ['Users']
            ]);
            if (in_array($this->Auth->user('id'), array_map(create_function('$o', 'return $o->id;'), $chat->users))) {
                $message->chats = [$chat];
                $message->date = date('Y-m-d H:i:s');
                $message->user_id = $this->Auth->user('id');
                if ($this->Messages->save($message)) {
                    //$this->Flash->success(__('The message has been saved.'));
                    return $this->redirect(['action' => 'view', $id]);
                } else {
                    $this->Flash->error(__('The message could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('You don\'t belong to this chat. You can\'t send any message in this chat.'));
            }
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chat = $this->Chats->newEntity();
        if ($this->request->is('post')) {
            $chat = $this->Chats->patchEntity($chat, $this->request->data);
            $this->loadModel('Users');
            $user = $this->Users->get($this->Auth->user('id'));
            if (!in_array($user, (array)$chat->users)) {
                $a = (array)$chat->users;
                array_push($a, $user);
                $chat->users = $a;
            }
            if ($this->Chats->save($chat)) {
                $this->Flash->success(__('The chat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat could not be saved. Please, try again.'));
            }
        }
        $messages = $this->Chats->Messages->find('list', ['limit' => 200]);
        $users = $this->Chats->Users->find('list', ['limit' => 200]);
        $this->set(compact('chat', 'messages', 'users'));
        $this->set('_serialize', ['chat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->isChatPrivate($id)) {
            $this->Flash->error(__('You don\'t belong to this chat. You can\'t access to the chat.'));
            return $this->redirect(['action' => 'index']);
        }

        $chat = $this->Chats->get($id, [
            'contain' => ['Messages', 'Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chat = $this->Chats->patchEntity($chat, $this->request->data);
            if ($this->Chats->save($chat)) {
                $this->Flash->success(__('The chat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat could not be saved. Please, try again.'));
            }
        }
        $messages = $this->Chats->Messages->find('list', ['limit' => 200]);
        $users = $this->Chats->Users->find('list', ['limit' => 200]);
        $this->set(compact('chat', 'messages', 'users'));
        $this->set('_serialize', ['chat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chat = $this->Chats->get($id);
        if ($this->Chats->delete($chat)) {
            $this->Flash->success(__('The chat has been deleted.'));
        } else {
            $this->Flash->error(__('The chat could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function table($id)
    {
        $chat = $this->Chats->get($id, [
            'contain' => ['Messages', 'Users']
        ]);

        $this->set('chat', $chat);
        $this->set('_serialize', ['chat']);
        echo $chat;
        die();
    }

    public function isChatPrivate($chatId) {
        $this->loadModel('ChatsUsers');
        $chatsUsers = $this->ChatsUsers->find()
            ->select(['chat_id'])
            ->where(['user_id' => $this->Auth->user('id'), 'chat_id' => $chatId])
            ->toArray()
        ;
        $chatsId = array_map(create_function('$o', 'return (int)$o->chat_id;'), $chatsUsers);

        if (empty($chatsId)) {
            return true;
        } else {
            return false;
        }
    }
}
