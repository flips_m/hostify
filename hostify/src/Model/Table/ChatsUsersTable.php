<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChatsUsers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Chats
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ChatsUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChatsUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChatsUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChatsUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChatsUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChatsUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChatsUser findOrCreate($search, callable $callback = null)
 */
class ChatsUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chats_users');
        $this->displayField('chat_id');
        $this->primaryKey(['chat_id', 'user_id']);

        $this->belongsTo('Chats', [
            'foreignKey' => 'chat_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['chat_id'], 'Chats'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
