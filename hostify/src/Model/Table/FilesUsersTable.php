<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FilesUsers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Files
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\FilesUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\FilesUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FilesUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FilesUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FilesUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FilesUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FilesUser findOrCreate($search, callable $callback = null)
 */
class FilesUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('files_users');
        $this->displayField('file_id');
        $this->primaryKey(['file_id', 'user_id']);

        $this->belongsTo('Files', [
            'foreignKey' => 'file_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['file_id'], 'Files'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
