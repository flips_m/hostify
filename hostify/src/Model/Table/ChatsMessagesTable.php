<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ChatsMessages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Chats
 * @property \Cake\ORM\Association\BelongsTo $Messages
 *
 * @method \App\Model\Entity\ChatsMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\ChatsMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ChatsMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ChatsMessage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ChatsMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ChatsMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ChatsMessage findOrCreate($search, callable $callback = null)
 */
class ChatsMessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chats_messages');
        $this->displayField('chat_id');
        $this->primaryKey(['chat_id', 'message_id']);

        $this->belongsTo('Chats', [
            'foreignKey' => 'chat_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Messages', [
            'foreignKey' => 'message_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['chat_id'], 'Chats'));
        $rules->add($rules->existsIn(['message_id'], 'Messages'));
        return $rules;
    }
}
